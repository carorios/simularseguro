package steps;

import cucumber.api.PendingException;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;
import pages.CotizarSeguroPages;
import pages.SimularSeguroAutoPages;
import pages.SimularSeguroIngresoPages;

public class SimularSeguroSteps {

    @Steps(shared = true)
    CotizarSeguroPages cotizarSeguroPages;
    @Steps(shared = true)
    SimularSeguroAutoPages simularSeguroAutoPages;
    @Steps(shared = true)
    SimularSeguroIngresoPages simularSeguroIngresoPages;

    @Given("^Hernan no tiene mucho tiempo por lo que ingresa a la pagina de Sura$")
    public void hernan_no_tiene_mucho_tiempo_por_lo_que_ingresa_a_la_pagina_de_Sura(){
            simularSeguroIngresoPages.ingreso();
        }
        
        @When("^Hernan ingresa a la opcion deseada y realiza la simulacion del seguro$")
        public void hernan_ingresa_a_la_opcion_deseada_y_realiza_la_simulacion_del_seguro() {
            simularSeguroAutoPages.simularSeguroParte1();
            simularSeguroAutoPages.tipoSeguro();
            cotizarSeguroPages.datosBasicos();
            cotizarSeguroPages.confirmarPlaca();
            cotizarSeguroPages.tipoVehiculo();
        }

        @Then("^La pagina procesa los datos ingresados y da un valor del seguro$")
        public void la_pagina_procesa_los_datos_ingresados_y_da_un_valor_del_seguro() {
            cotizarSeguroPages.detalleVehiculo();
            cotizarSeguroPages.verificarSimulacionSeguro();
        }
}
