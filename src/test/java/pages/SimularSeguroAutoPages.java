package pages;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.Select;

//@DefaultUrl("https://www.suraenlinea.com/vehiculos/seguro-vehiculos?utm_source=segurossura_personas&utm_medium=boton_home&utm_campaign=seguro_autos")
public class SimularSeguroAutoPages extends PageObject {

    @FindBy(name="placa")
    private WebElementFacade txtplaca;

    @FindBy(name="tipoServicio")
    private Select slctipoServicio;

    @FindBy(xpath="//*[@id=\"cotizador\"]/div/div/form/fieldset/div[1]/div/div[2]/a")
    private WebElementFacade btncotizarSeguro1;

    @FindBy(xpath = "//*[@id=\"cotizador\"]/div/div/form/fieldset/div[1]/div/a[2]")
    private WebElementFacade btntipoSeguro;

    public SimularSeguroAutoPages(WebDriver driver){super(driver);}

    public void simularSeguroParte1(){
        txtplaca.sendKeys("ABC123");
        slctipoServicio.selectByVisibleText("PARTICULAR");
        btncotizarSeguro1.click();
    }

    public void tipoSeguro(){
        btntipoSeguro.click();
    }
}
