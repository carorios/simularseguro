package pages;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.FindBy;

@DefaultUrl("https://www.segurossura.com.co/paginas/default.aspx")
public class SimularSeguroIngresoPages extends PageObject {

    @FindBy(xpath="//*[@id=\"menu-cotiza\"]/div/ul/li[2]/a/i")
    private WebElementFacade btnauto;

    public SimularSeguroIngresoPages(WebDriver driver){
        super(driver);
    }

    public void ingreso(){
        btnauto.click();
    }


}
