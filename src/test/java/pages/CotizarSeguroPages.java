package pages;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import org.hamcrest.MatcherAssert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.Select;

public class CotizarSeguroPages extends PageObject {

    @FindBy(name="placa")
    private WebElementFacade txtplaca;

    @FindBy(name="primerNombre")
    private WebElementFacade txtprimerNombre;

    @FindBy(name="primerApellido")
    private WebElementFacade txtprimerApellido;

    @FindBy(name="correo")
    private WebElementFacade txtcorreo;

    @FindBy(xpath = "//*[@id=\"cotizador\"]/div/div/form/fieldset/div/div[1]/div[2]/a")
    private WebElementFacade btncontinuar;

    //otro segmento
    @FindBy(xpath = "//*[@id=\"cotizador\"]/div/div/form/fieldset/div/div[1]/p")
    private WebElementFacade lblestaSeguro;

    @FindBy(xpath = "//*[@id=\"cotizador\"]/div/div/form/fieldset/div/div[1]/button[1]")
    private WebElementFacade btnsi;

    //otro segmento
    //@FindBy(name="placa") //la trae por defecto
    //private WebElementFacade txtplacaConfirmar;

    @FindBy(xpath="//*[@id=\"cotizador\"]/div/div/form/fieldset/div/div[1]/div[1]/sura-form[2]/div/div[1]/div/select")
    private Select slcclaseVehiculo;

    @FindBy(xpath="//*[@id=\"cotizador\"]/div/div/form/fieldset/div/div[1]/div[1]/sura-form[3]/div/div[1]/div/select")
    private Select slctipoServicio;

    @FindBy(xpath = "//*[@id=\"cotizador\"]/div/div/form/fieldset/div/div[1]/div[2]/a")
    private WebElementFacade btncontinuar2;

    //otro segmento
    @FindBy(name = "pasajeros")
    private WebElementFacade txtpasajeros;

    @FindBy(name = "modelo")
    private WebElementFacade txtmodelo;

    @FindBy(xpath = "//*[@id=\"cotizador\"]/div/div/form/fieldset/div/div[1]/div/sura-form[3]/div/div/div/select")
    private Select slccombustible;

    @FindBy(name = "cilindraje")
    private WebElementFacade txtcilindraje;

    @FindBy(xpath = "//*[@id=\"cotizador\"]/div/div/form/fieldset/div/div[1]/div/div/a")
    private WebElementFacade btncontinuar3;

    //otro segmento
    @FindBy(xpath = "/html/body/div[1]/div[2]/div/div/h2")
    private WebElementFacade lbltuSoat;

    @FindBy(xpath = "/html/body/div[1]/div[2]/div/div/div[1]/div[3]/div[1]/div/div[2]")
    private WebElementFacade lblvalor;

    @FindBy(xpath = "/html/body/div[1]/div[2]/div/div/div[1]/div[3]/div[1]/div/a")
    private WebElementFacade lnkdetalle;

    public CotizarSeguroPages(WebDriver driver){super(driver);}

    public void datosBasicos()
    {
        txtplaca.sendKeys("abc123");
        txtprimerNombre.sendKeys("Carolina");
        txtprimerApellido.sendKeys("Tejada");
        txtcorreo.sendKeys("correo@gmail.com");
        btncontinuar.click();
    }

    public void confirmarPlaca(){
        if(lblestaSeguro.isVisible()==true){
            btnsi.click();
        }
    }

    public void tipoVehiculo() {
        slcclaseVehiculo.selectByVisibleText("Bus");
        slctipoServicio.selectByVisibleText("Particular");
        btncontinuar2.click();
    }

    public void detalleVehiculo(){
        txtpasajeros.sendKeys("35");
        txtmodelo.sendKeys("2018");
        slccombustible.selectByVisibleText("GASOLINA");
        txtcilindraje.sendKeys("1200");
    }

    public void verificarSimulacionSeguro(){
        MatcherAssert.assertThat("Tu SOAT",lbltuSoat.isDisplayed());
        lnkdetalle.click();
    }
}
