package definitions;

import net.thucydides.core.annotations.Step;
import net.thucydides.core.steps.ScenarioSteps;
import org.fluentlenium.core.annotation.Page;
import pages.CotizarSeguroPages;
import pages.SimularSeguroAutoPages;
import pages.SimularSeguroIngresoPages;

public class SimularSeguroDefinitions {

    @Page
    CotizarSeguroPages cotizarSeguroPages;
    @Page
    SimularSeguroAutoPages simularSeguroAutoPages;
    @Page
    SimularSeguroIngresoPages simularSeguroIngresoPages;

    @Step
    public void abrirUrl() {
        simularSeguroIngresoPages.open();
    }

    @Step
    public void iniciarSimulacion(){
        simularSeguroAutoPages.simularSeguroParte1();
        simularSeguroAutoPages.tipoSeguro();
    }

    @Step
    public void confirmarTipoSeguroSimulacion(){
        simularSeguroAutoPages.tipoSeguro();
    }

    @Step
    public void datosBasicos(){
        cotizarSeguroPages.datosBasicos();
    }

    @Step
    public void confirmarPlaca(){
        cotizarSeguroPages.confirmarPlaca();
    }

    @Step
    public void tipoVehiculo(){
        cotizarSeguroPages.tipoVehiculo();
    }

    @Step
    public void detalleVehiculo(){
        cotizarSeguroPages.detalleVehiculo();
    }

    @Step
    public void verificarSimulacionSeguro(){
        cotizarSeguroPages.verificarSimulacionSeguro();
    }
}
