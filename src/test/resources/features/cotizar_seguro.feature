Feature: Cotizar seguro

  Con el fin de evitar contratiempos en caso de accidente
  Como persona preocupada por su seguridad y la de los demas
  Quiero comprar un seguro para mi automovil

  Scenario: Un conductor desea cotizar un seguro mediante la empresa Sura
    Given Hernan no tiene mucho tiempo por lo que ingresa a la pagina de Sura
    When Hernan ingresa a la opcion deseada y realiza la simulacion del seguro
    Then La pagina procesa los datos ingresados y da un valor del seguro